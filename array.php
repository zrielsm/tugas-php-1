<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>
    <?php
    echo "<h3> Soal 1 </h3>";
    $kids = ['Mike', 'Dustin', 'Will', 'Lucas', 'Max', 'Eleven'];
    $adults = ['Hopper', 'Nancy', 'Joyce', 'Jonathan', 'Murray'];
    echo "$kids[1] <br>";
    echo $adults[1];

    echo "<h3> Soal 2</h3>";
    $jmlkids = count($kids);
    $jmladults = count($adults);
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: $jmlkids";
    echo "<br>";
    echo "<ol>";
    echo "<li> $kids[0] </li>";
    echo "<li> $kids[1] </li>";
    echo "<li> $kids[2] </li>";
    echo "<li> $kids[3] </li>";
    echo "<li> $kids[4] </li>";
    echo "<li> $kids[5] </li>";
    
    echo "</ol>";
    echo "Total Adults: $jmladults"; 
    echo "<br>";
    echo "<ol>";
    echo "<li> $adults[0] </li>";
    echo "<li> $adults[1] </li>";
    echo "<li> $adults[2] </li>";
    echo "<li> $adults[3] </li>";
    echo "<li> $adults[4] </li>";

    echo "</ol>";

    echo "<h3>Soal 3</h3>";
    $data['0'] = "Name : Will Blayers <br>
                      Age : 12 <br>
                      Aliases : Will the wise <br>
                      Status : Alive";
    $data['1'] = "Name : Mike Wheeler <br>
                      Age : 12 <br>
                      Aliases : Dugeon Master <br>
                      Status : Alive";
    $data['2'] = "Name : Jim Hooper <br>
                      Age : 43 <br>
                      Aliases : Chief Hooper <br>
                      Status : Deceased";
    $data['3'] = "Name : Eleven <br>
                      Age : 12 <br>
                      Aliases : EL <br>
                      Status : Alive";
    echo "$data[2] <br>";
    echo $data[3];
    ?>
</body>

</html>