<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>

<body>
    <h1>Berlatih String PHP</h1>
    <?php
    echo "<h3> Soal No 1</h3>";
   
    echo "Tunjukkan dengan menggunakan echo berapa panjang dari string yang diberikan dan tunjukkan juga jumlah kata dalam kalimat <br>";

    $a = "PHP is never old";
    echo $a;
    $jukar = strlen($a);
    echo "<br>";
    echo "Panjang String : $jukar <br>";
    $jukat = str_word_count($a);
    echo "Jumlah kata : $jukat";
    $first_sentence = "Hello PHP!"; 
    $second_sentence = "I'm ready for the challenges";

    echo "<h3> Soal No 2</h3>";
    
    $string2 = "I love PHP";

    echo "<label>String: </label> \"$string2\" <br>";
    echo "Kata pertama: " . substr($string2, 0, 1) . "<br>";
  
    echo "Kata kedua: " . substr($string2, 2, 4) . "";
    echo "<br> Kata Ketiga: " . substr($string2, 6, 7) . "<br>";
    echo "<h3> Soal No 3 </h3>";
   
    $string3 = "PHP is old but Good!";
    echo "String: \"$string3\" <br>";
    
    echo "Output : " . substr($string3, 0, 14) . " awesome";
    ?>
</body>

</html>